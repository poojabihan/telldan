//
//  DetailViewController.m
//  ChangeItApp
//
//  Created by Tarun Sharma on 01/04/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "DetailViewController.h"
#import "MBProgressHUD.h"
#import "constant.h"
#import "ImageViewController.h"
#import "UIImageView+WebCache.h"


@interface DetailViewController ()
{
    MBProgressHUD * hud;
    
}
@end

@implementation DetailViewController
@synthesize date,details,imageStr,locationStr,imageVw,dateAndTime,detailText,indicator,locationLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Details"];

    // Do any additional setup after loading the view.
    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if(internetStatus != NotReachable)
//    {
//    NSURLSession *session=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    
//        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        
//        // Set the label text.
//        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
//
//    [[session dataTaskWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageStr]] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//      {
//          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//              
//              imageVw.image  = [UIImage imageWithData:data];
//              
//                  [hud hideAnimated:YES];
//              
//              
//          }];
//      }] resume];
//    
//    dateAndTime.text = date;
//    detailText.text = details;
//    locationLabel.text=locationStr;
//        
//    }
//    else
//    {
    self.imageVw.clipsToBounds = YES;
    
    self.imageVw.contentMode = UIViewContentModeScaleAspectFit;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![_imageName isEqualToString:@"noImage"])
        {
            //imageVw.image=[UIImage imageWithData:_imageData];
            //NSString * imageString=[[NSString stringWithFormat:@"%@%@",kImageURL, _imageName]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
             NSString * imageString = [[NSString stringWithFormat:@"%@%@",kImageURL, _imageName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageString].absoluteString];
            if(image == nil)
            {
                [self.imageVw sd_setImageWithURL:[NSURL URLWithString:imageString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [self.imageVw setImage:image];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [self.imageVw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        
                    }
                }];
            } else {
                [self.imageVw setImage:image];
                //[activityIndicator removeFromSuperview];
            }

            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(singleTapGestureCaptured)];
            [imageVw addGestureRecognizer:singleTap];
            [imageVw setMultipleTouchEnabled:YES];
            [imageVw setUserInteractionEnabled:YES];

            
        }
        else{
           imageVw.image=[UIImage imageNamed:@"ImgNotFound.png"];
        }
        
        dateAndTime.text = date;
        detailText.text = details;
        locationLabel.text=locationStr;
        
    });
    
    
       
    //}
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)singleTapGestureCaptured{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    //hud.dimBackground = YES;
    
    ImageViewController *image = [self.storyboard instantiateViewControllerWithIdentifier:@"image"];
    image.imageVariable=imageVw.image;
    [self.navigationController pushViewController:image animated:YES];
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    
    
}


@end
