//
//  InboxViewController.m
//  TellDan
//
//  Created by Tarun Sharma on 07/09/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "InboxViewController.h"
#import "AppDelegate.h"
#import "constant.h"
#import <CoreData/CoreData.h>
#import <MBProgressHUD.h>
#import <AFNetworking/AFNetworking.h>

@interface InboxViewController ()
{
    NSArray * dictArray, * itemChatArray,*resultArray;
    NSString *msgLength, *strUrl;
    NSMutableDictionary *dict;
    //UIRefreshControl *refreshControl;
    InboxItemTableViewCell * inboxCell;
    MBProgressHUD * hud;
     BOOL flag;
    
}
@end
NSManagedObjectContext *inboxContext, * chatContextInbox;
;

@implementation InboxViewController

#pragma mark ViewLifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:@"Messages"];
    flag=YES;
   

    [self.inboxTableView registerNib:[UINib nibWithNibName:@"InboxItemTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    inboxContext=[UIAppDelegate managedObjectContext];
    chatContextInbox=[UIAppDelegate managedObjectContext];
    
        self.inboxTableView.dataSource = self;
    self.inboxTableView.delegate = self;
//    refreshControl = [[UIRefreshControl alloc] init];
//    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
//    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//
//    [self.inboxTableView addSubview:refreshControl];
    
    

}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        
        [self loadingElementsInInboxWithBackgroundThread];
        
        
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    if (flag==YES) {
        [self loadingElementsInInbox];
        flag=NO;
    }
 else{
       //[self performSelectorInBackground:@selector(loadingElementsInInboxWithBackgroundThread) withObject:nil];
     [self loadingElementsInInboxWithBackgroundThread];
   }
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    [hud hideAnimated:YES];
}

-(void)loadingElementsInInboxWithBackgroundThread{
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[self.view setUserInteractionEnabled:NO];

//        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//        [activity setColor:khudColour];
//        
//       // [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
//        activity.center = self.navigationController.view.center;
//
//
//        [self.view addSubview:activity];
//        [activity startAnimating];
        NSString *str;
        dictArray = [[NSArray alloc]init];
        itemChatArray= [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id in inbox %@",emailIdString);
        if (emailIdString==NULL) {
            str= [NSString stringWithFormat:@"getinboxitem?email_id="];
        }
        else{
            str = [NSString stringWithFormat:@"getinboxitem?email_id=%@",emailIdString];
        }
        
        
        NSURL *baseURL = [NSURL URLWithString:kBaseURL];
        dispatch_queue_t myQueue = dispatch_queue_create("com.Chetaru.TellDan.loadingElementsInInboxWithBackgroundThread", DISPATCH_QUEUE_SERIAL);
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        [manager setCompletionQueue:myQueue];

        
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject objectForKey:@"response"]) {
                dictArray = [responseObject objectForKey:@"response"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.inboxTableView reloadData];
                    //[activity stopAnimating];
                    //[self.view setUserInteractionEnabled:YES];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                    
                });
                
                NSArray * arr;
                BOOL foo = true;
                unsigned long j=[self fetchingCoreData];
                for (NSMutableDictionary *dictInFor in dictArray) {
                    arr=[dictInFor objectForKey:@"itemchat"];
                    //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);
                    unsigned long i=[self fetchingCoreDataInChat:[dictInFor objectForKey:@"changeit_id"]];
                    if (i<arr.count ||i>arr.count|| j<dictArray.count||j>dictArray.count) {
                        foo=false;
                    }
                    
                }
                NSLog(@"Inbox Wow %d ",foo);
                if (foo==false) {
                    [self deleteAllObjects:@"Inbox"];
                    [self deleteAllObjectsInChat:@"Chat"];
                    [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        [inboxContext performBlockAndWait:^{
                            NSManagedObject * inbox = [NSEntityDescription insertNewObjectForEntityForName:@"Inbox" inManagedObjectContext:inboxContext];
                            
                            
                            [inbox setValue:[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"] forKey:@"lastmsg"];
                            [inbox setValue:[obj objectForKey:@"msg_detail"] forKey:@"msgdetail"];
                            
                            [inbox setValue:[obj objectForKey:@"msg_type"] forKey:@"msgtype"];
                            [inbox setValue:[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"] forKey:@"createddate"];
                            [inbox setValue:[obj objectForKey:@"changeit_id"] forKey:@"changeitid"];
                            itemChatArray=[obj objectForKey:@"itemchat"];
                            
                            
                            
                            [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                
                                if (chatContextInbox != nil) {
                                    [chatContextInbox performBlockAndWait:^{
                                        NSError *error;
                                        
                                        
                                        
                                        
                                        NSManagedObject * chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:chatContextInbox];
                                        
                                        [chat setValue:[obj objectForKey:@"changeit_id"] forKey:@"changeitid"];
                                        [chat setValue:[chatObj objectForKey:@"created_date"] forKey:@"createddate"];
                                        [chat setValue:[chatObj objectForKey:@"message"] forKey:@"message"];
                                        [chat setValue:[chatObj objectForKey:@"post_type"] forKey:@"posttype"];
                                        [chat setValue:[chatObj objectForKey:@"user_type"] forKey:@"usertype"];
                                        
                                        
                                        
                                        
                                        if (![chatContextInbox save:&error]) {
                                            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                        }
                                        
                                        
                                    }];
                                }
                                
                                
                                
                            }];
                            
                            
                            
                            NSError *error;
                            
                            
                            if (![inboxContext save:&error]) {
                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                //[activity stopAnimating];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                });
                                
                                
                            }
                        }];
                    }];
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[activity stopAnimating];
                    //[self.view setUserInteractionEnabled:YES];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                });
                [self deleteAllObjects:@"Inbox"];
                [self deleteAllObjectsInChat:@"Chat"];
            }
            
            

            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                //[activity stopAnimating];
                //[self.view setUserInteractionEnabled:YES];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            [self deleteAllObjects:@"Inbox"];
            [self deleteAllObjectsInChat:@"Chat"];

        }];
        
    }
    
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            //Any UI updates should be made here .
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });

        [inboxContext performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Inbox" inManagedObjectContext:inboxContext];
            [fetchRequest setEntity:entity];
            NSError *error;
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:NO];
            
            [fetchRequest setSortDescriptors:@[sortDescriptor]];
            
            
            dictArray = [inboxContext executeFetchRequest:fetchRequest error:&error];
        }];
        
    }
 }


-(void)loadingElementsInInbox{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        NSString *str;
        dictArray = [[NSArray alloc]init];
        itemChatArray= [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id in inbox %@",emailIdString);
        if (emailIdString==NULL) {
            str= [NSString stringWithFormat:@"getinboxitem?email_id="];
        }
        else{
            str = [NSString stringWithFormat:@"getinboxitem?email_id=%@",emailIdString];
        }
        
        
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
        
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        
        [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"response = %@", responseObject);
             if ([responseObject objectForKey:@"response"]) {
                 dictArray = [responseObject objectForKey:@"response"];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES];
                     [self.inboxTableView reloadData];
                 });
                 
                 //[refreshControl endRefreshing];
                 NSArray * arr;
                 BOOL foo = true;
                 unsigned long j=[self fetchingCoreData];
                 for (NSMutableDictionary *dictInFor in dictArray) {
                     arr=[dictInFor objectForKey:@"itemchat"];
                     //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);
                     unsigned long i=[self fetchingCoreDataInChat:[dictInFor objectForKey:@"changeit_id"]];
                     if (i<arr.count ||i>arr.count|| j<dictArray.count||j>dictArray.count) {
                         foo=false;
                     }
                     
                 }
                 NSLog(@"Inbox Wow %d ",foo);
                 if (foo==false) {
                     [self deleteAllObjects:@"Inbox"];
                     [self deleteAllObjectsInChat:@"Chat"];
                     [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                         
                         [inboxContext performBlockAndWait:^{
                             NSManagedObject * inbox = [NSEntityDescription insertNewObjectForEntityForName:@"Inbox" inManagedObjectContext:inboxContext];
                         
                             
                             [inbox setValue:[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"] forKey:@"lastmsg"];
                             [inbox setValue:[obj objectForKey:@"msg_detail"] forKey:@"msgdetail"];
                             
                             [inbox setValue:[obj objectForKey:@"msg_type"] forKey:@"msgtype"];
                             [inbox setValue:[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"] forKey:@"createddate"];
                             [inbox setValue:[obj objectForKey:@"changeit_id"] forKey:@"changeitid"];
                             
                             
                             
                             itemChatArray=[obj objectForKey:@"itemchat"];
                             
                             
                             
                             [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                 
                                 if (chatContextInbox != nil) {
                                     [chatContextInbox performBlockAndWait:^{
                                         NSError *error;
                                         
                                         
                                         
                                         
                                         NSManagedObject * chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:chatContextInbox];
                                         
                                         [chat setValue:[obj objectForKey:@"changeit_id"] forKey:@"changeitid"];
                                         [chat setValue:[chatObj objectForKey:@"created_date"] forKey:@"createddate"];
                                         [chat setValue:[chatObj objectForKey:@"message"] forKey:@"message"];
                                         [chat setValue:[chatObj objectForKey:@"post_type"] forKey:@"posttype"];
                                         [chat setValue:[chatObj objectForKey:@"user_type"] forKey:@"usertype"];
                                         
                                         
                                         
                                         
                                         if (![chatContextInbox save:&error]) {
                                             NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                         }
                                         
                                         
                                     }];
                                 }
                                 
                                 
                                 
                             }];
                             
                             
                             
                             NSError *error;
                             
                             
                             if (![inboxContext save:&error]) {
                                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                             }
                         
                         }];
                         
                     }];

                 }
             
             }
            
          
            else{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    //[refreshControl endRefreshing];
                    
                });
                [self deleteAllObjects:@"Inbox"];
                [self deleteAllObjectsInChat:@"Chat"];


            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                
                
                //[refreshControl endRefreshing];
            });
            [self deleteAllObjects:@"Inbox"];
            [self deleteAllObjectsInChat:@"Chat"];

            
        }];
        
        
        
        
        }
    
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            //[refreshControl endRefreshing];
        });
        
      [inboxContext performBlockAndWait:^{
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Inbox" inManagedObjectContext:inboxContext];
                [fetchRequest setEntity:entity];
                NSError *error;
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:NO];
                
                [fetchRequest setSortDescriptors:@[sortDescriptor]];
                
                
               dictArray = [inboxContext executeFetchRequest:fetchRequest error:&error];
      }];

    }
    

}



//#pragma mark PullToRefresh Method
//
//
//-(void)refreshData{
//    [self loadingElementsInInbox];
//}

#pragma mark Fetching Coredata Methods

-(unsigned long)fetchingCoreData{
   __block NSArray * coreDataArray;
    [inboxContext performBlockAndWait:^{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Inbox" inManagedObjectContext:inboxContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    
   coreDataArray = [inboxContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
    }];
    return coreDataArray.count;
}

-(unsigned long)fetchingCoreDataInChat:(NSString *)changeID{
     [chatContextInbox performBlockAndWait:^{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chat" inManagedObjectContext:chatContextInbox];
    [fetchRequest setEntity:entity];
    NSError *error;
    
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"changeitid == %@", changeID]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    resultArray = [chatContextInbox executeFetchRequest:fetchRequest error:&error];
     }];
    return resultArray.count;
    
}


#pragma mark Deleting Coredata Methods

- (void) deleteAllObjects: (NSString *) entityDescription  {
     [inboxContext performBlockAndWait:^{
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:inboxContext];
            [fetchRequest setEntity:entity];
            
            NSError *error;
            NSArray *items = [inboxContext executeFetchRequest:fetchRequest error:&error];
            //[fetchRequest release];
            
            
            for (NSManagedObject *managedObject in items) {
                [inboxContext deleteObject:managedObject];
                NSLog(@"%@ object deleted",entityDescription);
            }
            if (![inboxContext save:&error]) {
                NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
            }
     }];
    
    
}

- (void) deleteAllObjectsInChat: (NSString *) entityDescription  {
    
     [chatContextInbox performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:chatContextInbox];
            [fetchRequest setEntity:entity];
            NSError *error;
            
            NSArray *items = [chatContextInbox executeFetchRequest:fetchRequest error:&error];
            //[fetchRequest release];
            
            [chatContextInbox performBlockAndWait:^{
                for (NSManagedObject *managedObject in items) {
                    [chatContextInbox deleteObject:managedObject];
                    NSLog(@"%@ object deleted",entityDescription);
                }
                
            }];
            if (![chatContextInbox save:&error]) {
                NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
            }
     }];
    
      

}

#pragma mark Gesture Method

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}
#pragma mark TableViewDelegate Methods



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dictArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    inboxCell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (inboxCell==nil) {
        inboxCell=[[InboxItemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }

    //inboxCell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    //[inboxCell setSelectionStyle:UITableViewCellSelectionStyleNone];

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
    dict = [dictArray objectAtIndex:indexPath.row];

    inboxCell.lastMessageLabel.text=[[[dict objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"];
    inboxCell.titleHeadLineLabel.text = [dict objectForKey:@"msg_detail"];
    if ([[dict objectForKey:@"msg_type"] isEqualToString:@"ann"]) {
        inboxCell.timeLabel.text =[NSString stringWithFormat:@"TellDan : %@ ",[[[dict objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"]];
        
    }else{
        inboxCell.timeLabel.text =[NSString stringWithFormat:@"TellDan : %@  (Item %@)",[[[dict objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"],[dict objectForKey:@"changeit_id"]];
    }
    }
    else{
        NSManagedObject *info = [dictArray objectAtIndex:indexPath.row];
        inboxCell.lastMessageLabel.text=[info valueForKey:@"lastmsg"];
        inboxCell.titleHeadLineLabel.text = [info valueForKey:@"msgdetail"];
        if ([[info valueForKey:@"msgtype"] isEqualToString:@"ann"]) {
            inboxCell.timeLabel.text =[NSString stringWithFormat:@"TellDan : %@ ",[info valueForKey:@"createddate"]];
            
        }else{
            inboxCell.timeLabel.text =[NSString stringWithFormat:@"TellDan : %@  (Item %@)",[info valueForKey:@"createddate"],[info valueForKey:@"changeitid"]];
        }

    }
   
    
    return inboxCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
    NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
     ChatViewController *chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
    chatController.itemID=[rowDict objectForKey:@"changeit_id"];
    NSLog(@"chang id in inbox %@",[rowDict objectForKey:@"changeit_id"]);
   
        dispatch_async(dispatch_get_main_queue(), ^{
           [self.navigationController pushViewController:chatController animated:YES];
        });
    
    }
    else
    {
        NSManagedObject * info = [dictArray objectAtIndex:indexPath.row];
        ChatViewController *chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
        chatController.itemID=[info valueForKey:@"changeitid"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:chatController animated:YES];
        });
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
