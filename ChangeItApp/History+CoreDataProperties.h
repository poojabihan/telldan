//
//  History+CoreDataProperties.h
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "History+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface History (CoreDataProperties)

+ (NSFetchRequest<History *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *locationdetail;
@property (nullable, nonatomic, copy) NSString *msgdetail;

@end

NS_ASSUME_NONNULL_END
