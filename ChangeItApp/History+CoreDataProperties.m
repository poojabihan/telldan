//
//  History+CoreDataProperties.m
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "History+CoreDataProperties.h"

@implementation History (CoreDataProperties)

+ (NSFetchRequest<History *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"History"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic image;
@dynamic locationdetail;
@dynamic msgdetail;

@end
