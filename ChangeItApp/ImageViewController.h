//
//  ImageViewController.h
//  TellDan
//
//  Created by Tarun Sharma on 10/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property UIImage * imageVariable;

@end
