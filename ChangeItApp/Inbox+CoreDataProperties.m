//
//  Inbox+CoreDataProperties.m
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "Inbox+CoreDataProperties.h"

@implementation Inbox (CoreDataProperties)

+ (NSFetchRequest<Inbox *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Inbox"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic lastmsg;
@dynamic msgdetail;
@dynamic msgtype;

@end
