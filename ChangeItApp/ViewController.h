//
//  ViewController.h
//  ChangeItApp
//
//  Created by Tarun Sharma on 21/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AFNetworking/AFNetworking.h>

#import "InboxViewController.h"
#import "SCLAlertView.h"

@interface ViewController : UIViewController<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate, CLLocationManagerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UITextView *commentTxtView;

@property (weak, nonatomic) IBOutlet UIButton *rghtButton;

@property (weak, nonatomic) IBOutlet UITextField *locationText;

@property (weak, nonatomic) IBOutlet UIImageView *imageVw;

@property (weak, nonatomic) IBOutlet UITextField *textField1;




- (IBAction)onHistory:(id)sender;

- (IBAction)takeAndAddPhoto:(id)sender;

- (IBAction)onSendingMail:(id)sender;

- (IBAction)sendLocation:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *historyButton;
-(void)sendingMail;
- (IBAction)deleteButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
- (IBAction)inboxButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *inboxButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollInVC;
@property  NSMutableDictionary * serverDataDict;
@property NSString * string,* URLString;
@property (weak, nonatomic) IBOutlet UIView *viewOnSV;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *takeAndAddButtonInstance;
- (BOOL)validateEmailWithString:(NSString*)checkString;
@end

