//
//  Inbox+CoreDataProperties.h
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "Inbox+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Inbox (CoreDataProperties)

+ (NSFetchRequest<Inbox *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *lastmsg;
@property (nullable, nonatomic, copy) NSString *msgdetail;
@property (nullable, nonatomic, copy) NSString *msgtype;

@end

NS_ASSUME_NONNULL_END
