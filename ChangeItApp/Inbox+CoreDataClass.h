//
//  Inbox+CoreDataClass.h
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Inbox : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Inbox+CoreDataProperties.h"
