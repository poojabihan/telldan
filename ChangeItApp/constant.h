//
//  constant.h
//  TellDan
//
//  Created by Tarun Sharma on 10/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#ifndef constant_h
#define constant_h
#define UIAppDelegate \
((AppDelegate*)[[UIApplication sharedApplication]delegate])
//constants for Webservice
//telldan
//tarun_demo

#define kBaseURL @"http://tellden.softintelligence.co.uk/index.php/api/"
//#define kBaseURL @"http://telldan.softintelligence.co.uk/index.php/api/"
#define kImageURL @"http://tellden.softintelligence.co.uk/assets/upload/telldan/"
#define kChatImageURL @"http://tellden.softintelligence.co.uk/assets/upload/chatimg/"
#define kAppNameAPI @"TellDan"
#define khudColour [UIColor colorWithRed:48/255.0f green:135/255.0f blue:180/255.0f alpha:1]
#define kAppNameAlert @"Tell Dan"
#define kInternetOff @"Internet Connection Required!"
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


#endif /* constant_h */
