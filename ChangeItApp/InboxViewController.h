//
//  InboxViewController.h
//  TellDan
//
//  Created by Tarun Sharma on 07/09/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InboxItemTableViewCell.h"
#import "ChatViewController.h"
#import "Reachability.h"
@interface InboxViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *inboxTableView;
-(void)loadingElementsInInbox;
@end
