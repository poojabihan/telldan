//
//  Chat+CoreDataClass.h
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Chat : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Chat+CoreDataProperties.h"
