//
//  ChatImageViewController.h
//  TellDan
//
//  Created by Tarun Sharma on 21/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatImageViewController : UIViewController<UIGestureRecognizerDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *chatImage;
@property NSString * imageURL;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end
