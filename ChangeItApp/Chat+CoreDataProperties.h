//
//  Chat+CoreDataProperties.h
//  TellDan
//
//  Created by Tarun Sharma on 25/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "Chat+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Chat (CoreDataProperties)

+ (NSFetchRequest<Chat *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *posttype;
@property (nullable, nonatomic, copy) NSString *usertype;

@end

NS_ASSUME_NONNULL_END
