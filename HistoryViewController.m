//
//  HistoryViewController.m
//  ChangeItApp
//
//  Created by Tarun Sharma on 26/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "ViewController.h"
#import "constant.h"
#import "UIImageView+WebCache.h"
#import <CoreData/CoreData.h>

@interface HistoryViewController ()
{
    NSArray *dictArray;
    NSString *msgLength, *strUrl;
    NSMutableDictionary *dict;
    NSString *str;
    NSArray *fetchedObjects;
    MBProgressHUD *hud;
}
@end
NSManagedObjectContext *context;
@implementation HistoryViewController



#pragma mark ViewLifeCycleMethods

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"History"];
   
    [self.tableview registerNib:[UINib nibWithNibName:@"HistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    context=[UIAppDelegate managedObjectContext];
    [self loadingElementsInHistory];
    
    
    
}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        [self loadingElementsInHistory];
        
        
    }
    
}
-(void)loadingElementsInHistory{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {

        dictArray = [[NSArray alloc]init];
        

        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
   
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSString * path = [NSString stringWithFormat:@"getuserrecord?device_id=%@&app_name=%@&email_id_to=%@",[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI,emailIdString];
        NSURL *baseURL = [NSURL URLWithString:kBaseURL];
    
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"response = %@", responseObject);
             if ([responseObject objectForKey:@"response"]) {
                 dictArray = [responseObject objectForKey:@"response"];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES];
                     [self.tableview reloadData];
                 });
                 NSLog(@"Count in Online Service %lu",(unsigned long)dictArray.count);
                 unsigned long i=[self fetchingCoreData];
                 NSLog(@"Count %lu",i);
                 
                 if (dictArray.count<i || dictArray.count>i) {
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         [self deleteAllObjects:@"History"];
                         [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                             
                             [context performBlockAndWait:^{
                                 
                                 NSManagedObject * history = [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:context];
                                 
                                 
                                 [history setValue:[obj objectForKey:@"msg_detail"] forKey:@"msgdetail"];
                                 [history setValue:[obj objectForKey:@"location_detail"] forKey:@"locationdetail"];
                                 
                                 [history setValue:[obj objectForKey:@"created_date"] forKey:@"createddate"];
                                 [history setValue:[obj objectForKey:@"changeit_id"] forKey:@"changeitid"];
                                 
                                 
                                 
                                 
                                 if ([[obj objectForKey:@"images"]isEqualToString:@""]) {
                                     [history setValue:@"noImage" forKey:@"image"];
                                     //history.image=@"noImage";
                                 }
                                 else{
                                     [history setValue:[obj objectForKey:@"images"] forKey:@"image"];
                                     //history.image=[obj objectForKey:@"images"];
                                 }
                                 
                                 
                                 
                                 
                                 
                             }];
                             
                             NSError *error;
                             
                             if (![context save:&error]) {
                                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                             }
                             
                         }];
                         
                     });
                     
                 }
             }
           
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self deleteAllObjects:@"History"];
        
            }

            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            [self deleteAllObjects:@"History"];

            
        }];
        
                
        

}
else{
            // Test listing all Stored Messages from the DB
    
            [context performBlockAndWait:^{
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"History" inManagedObjectContext:context];
                [fetchRequest setEntity:entity];
                NSError *error;
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                                    initWithKey:@"createddate" ascending:NO];
                [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
                dictArray = [context executeFetchRequest:fetchRequest error:&error];
                NSLog(@"Count in coredata %lu",(unsigned long)dictArray.count);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self.tableview reloadData];
                });
                
            }];
           
        }
    

    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if(internetStatus != NotReachable)
//    {
//        
//        dictArray = [[NSArray alloc]init];
//        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
//        
//        // getting an NSString
//        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
//        NSLog(@"email id in history %@",emailIdString);
//        if (emailIdString==NULL) {
//            str = [NSString stringWithFormat:@"%@getuserrecord?device_id=%@&app_name=%@&email_id_to=",kBaseURL,[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI];
//        }
//        else{
//            str = [NSString stringWithFormat:@"%@getuserrecord?device_id=%@&app_name=%@&email_id_to=%@",kBaseURL,[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI,emailIdString];
//        }
//        
//        
//        
//        
//        
//        NSURL *url = [NSURL URLWithString:str];
//        
//        NSData *urlData = [NSData dataWithContentsOfURL:url];
//        
//        NSError *err;
//        
//        NSMutableDictionary *rootDict = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingMutableContainers error:&err];
//        
//        dictArray = [rootDict objectForKey:@"response"];
//        NSLog(@"response %@",rootDict);
//        [self.tableview reloadData];
//        
//        NSLog(@"Count in Online Service %lu",(unsigned long)dictArray.count);
//        unsigned long i=[self fetchingCoreData];
//        NSLog(@"Count %lu",i);
//        
//        if (dictArray.count<i || dictArray.count>i) {
//            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                [self deleteAllObjects:@"History"];
//                [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    
//                    [context performBlockAndWait:^{
//                    
//                        NSManagedObject * history = [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:context];
//                        
//                        
//                        [history setValue:[obj objectForKey:@"msg_detail"] forKey:@"msgdetail"];
//                        [history setValue:[obj objectForKey:@"location_detail"] forKey:@"locationdetail"];
//                        
//                        [history setValue:[obj objectForKey:@"created_date"] forKey:@"createddate"];
//                        [history setValue:[obj objectForKey:@"changeit_id"] forKey:@"changeitid"];
//                        
//                        
//                        
//                        
//                        if ([[obj objectForKey:@"images"]isEqualToString:@""]) {
//                            [history setValue:@"noImage" forKey:@"image"];
//                            //history.image=@"noImage";
//                        }
//                        else{
//                            [history setValue:[obj objectForKey:@"images"] forKey:@"image"];
//                            //history.image=[obj objectForKey:@"images"];
//                        }
// 
//                        
//                        
//                        
//                
//                    }];
//                    
//                    NSError *error;
//                    
//                    if (![context save:&error]) {
//                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
//                    }
//                    
//                }];
//                
//            });
//            
//        }
    
//    }
//    else{
//        // Test listing all Stored Messages from the DB
//        [context performBlockAndWait:^{
//            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//            NSEntityDescription *entity = [NSEntityDescription entityForName:@"History" inManagedObjectContext:context];
//            [fetchRequest setEntity:entity];
//            NSError *error;
//            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
//                                                initWithKey:@"createddate" ascending:NO];
//            [fetchRequest setSortDescriptors:@[sortDescriptor]];
//            
//            dictArray = [context executeFetchRequest:fetchRequest error:&error];
//            NSLog(@"Count in coredata %lu",(unsigned long)dictArray.count);
//        }];
//       
//    }
//
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    [hud hideAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Gesture Method

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark FetchingCoreData method

-(unsigned long)fetchingCoreData{
    __block NSArray * coreDataArray;
    [context performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"History" inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        coreDataArray = [context executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
    }];
    
    return coreDataArray.count;
}

#pragma mark Deleting Coredata method
- (void) deleteAllObjects: (NSString *) entityDescription  {
    
    [context performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        
        for (NSManagedObject *managedObject in items) {
            [context deleteObject:managedObject];
            NSLog(@"%@ object deleted",entityDescription);
        }
        if (![context save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
        
    }];
    
    
}

#pragma mark TableViewMethods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dictArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[HistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    cell.tag = indexPath.row;

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
       
        dict = [dictArray objectAtIndex:indexPath.row];
        if ([[dict objectForKey:@"images"] isEqualToString:@""]) {
            
            UIImage *tmpImage = [UIImage imageNamed:@"ImgNotFound.png"];
            cell.imagevw.image = tmpImage;
            
        }
        else{
            cell.imagevw.image=nil;
                        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                        [activityIndicator setCenter: cell.imagevw.center];
                        [activityIndicator startAnimating];
                        [cell.contentView addSubview:activityIndicator];
                    //strUrl =[[NSString stringWithFormat:@"%@%@",kImageURL, [dict objectForKey:@"images"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            strUrl= [[NSString stringWithFormat:@"%@%@",kImageURL, [dict objectForKey:@"images"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:strUrl].absoluteString];
            if(image == nil)
            {
                [cell.imagevw sd_setImageWithURL:[NSURL URLWithString:strUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [cell.imagevw setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [cell.imagevw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                    }
                }];
            } else {
                [cell.imagevw setImage:image];
                [activityIndicator stopAnimating];
                //[activityIndicator removeFromSuperview];
            }

            
//                    NSURLSession *session=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//            
//                    [[session dataTaskWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                      {
//                          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            
//                              cell.imagevw.image  = [UIImage imageWithData:data];
//                              [activityIndicator stopAnimating];
//                              
//                          }];
//                      }] resume];
                    }

        
        cell.dataText.text = [dict objectForKey:@"msg_detail"];
        cell.textOfLocation.text = [dict objectForKey:@"location_detail"];
        cell.dateAndTime.text = [dict objectForKey:@"created_date"];
        
        cell.myButton.tag= [[dict objectForKey:@"changeit_id"] integerValue];
        [cell.myButton addTarget:self action:@selector(resendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.chatButton.tag=[[dict objectForKey:@"changeit_id"] integerValue];
        [cell.chatButton addTarget:self action:@selector(sendQueryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
    
        
        
        NSManagedObject * info = [dictArray objectAtIndex:indexPath.row];
        //cell.textLabel.text = obj. name;
        // Configure the cell...
                //dict = [fetchedObjects objectAtIndex:indexPath.row];
        cell.dataText.text =[info valueForKey:@"msgdetail"];
        
        cell.textOfLocation.text =[info valueForKey:@"locationdetail"];
        cell.dateAndTime.text =[info valueForKey:@"createddate"];
        NSLog(@"Message %@",[info valueForKey:@"msgdetail"]);
        
        cell.myButton.tag= [[info valueForKey:@"changeitid"] integerValue];
        [cell.myButton addTarget:self action:@selector(resendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.chatButton.tag=[[info valueForKey:@"changeitid"] integerValue];
        [cell.chatButton addTarget:self action:@selector(sendQueryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        NSLog(@"Info Image %@",[info valueForKey:@"image"]);
        if (![[info valueForKey:@"image"] isEqualToString:@"noImage"])
        {
           // cell.imagevw.image  = [UIImage imageWithData:info.image];
            cell.imagevw.image=nil;
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator setCenter: cell.imagevw.center];
            [activityIndicator startAnimating];
            [cell.contentView addSubview:activityIndicator];
            //strUrl =[[NSString stringWithFormat:@"%@%@",kImageURL, [info valueForKey:@"image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            strUrl= [[NSString stringWithFormat:@"%@%@",kImageURL, [info valueForKey:@"image"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:strUrl].absoluteString];
            if(image == nil)
            {
                [cell.imagevw sd_setImageWithURL:[NSURL URLWithString:strUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [cell.imagevw setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [cell.imagevw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                        
                    }
                }];
            } else {
                [cell.imagevw setImage:image];
                [activityIndicator stopAnimating];
                //[activityIndicator removeFromSuperview];
            }

            
        }
        else{
            cell.imagevw.image=[UIImage imageNamed:@"ImgNotFound.png"];
        }
        
       
       
  
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    // hud.dimBackground = YES;

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
           
                
                
                DetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
                
                NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
                
                //NSString * imageString=[NSString stringWithFormat:@"%@%@",kImageURL, [rowDict objectForKey:@"images"]];
                //NSURL * url=[NSURL URLWithString:imageString];
                //NSString * imageString= [rowDict objectForKey:@"images"];
                if ([[rowDict objectForKey:@"images"] isEqualToString:@""]) {
                    detail.imageName=@"noImage";
                }
                else{
                    detail.imageName = [rowDict objectForKey:@"images"];

                }
                //NSLog(@"imagename%@Hello",imageString);
                NSLog(@"RowDict %@",rowDict);

                
                detail.date = [rowDict objectForKey:@"created_date"];
                detail.details = [rowDict objectForKey:@"msg_detail"];
                detail.locationStr=[rowDict objectForKey:@"location_detail"];
                 dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:detail animated:YES];

                
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            });
            
        });

   
    }
    else{
        
        DetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
        NSManagedObject *info = [dictArray objectAtIndex:indexPath.row];
        //NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
        
        detail.imageName = [info valueForKey:@"image"];
        NSLog(@"Core Data image %@",[info valueForKey:@"image"]);
        detail.date = [info valueForKey:@"createddate"];
        detail.details = [info valueForKey:@"msgdetail"];
        detail.locationStr=[info valueForKey:@"locationdetail"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            [self.navigationController pushViewController:detail animated:YES];
        });
        

        
    }
}

#pragma mark Resend & SendQueryMethods


-(void)sendQueryButtonClick:(id)sender
{
    
    
        UIButton *senderButton = (UIButton *)sender;
        
        ChatViewController * chatController=[[ChatViewController alloc]init];
        chatController.itemID=[NSString stringWithFormat:@"%ld",(long)senderButton.tag];
        NSLog(@"sender button %@",chatController.itemID);
        //[self presentViewController:chatController animated:YES completion:nil];
    dispatch_async(dispatch_get_main_queue(), ^{

        [self.navigationController pushViewController:chatController animated:YES];
    });
    
}
-(void)resendButtonClick:(id)sender{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Are you sure to Resend?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * resendAction=[UIAlertAction actionWithTitle:@"Resend" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
            hud.contentColor =khudColour;
            hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
            //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
            
            // Set the label text.
            hud.label.text = NSLocalizedString(@"Sending...", @"HUD loading title");

            UIButton *senderButton = (UIButton *)sender;
            NSLog(@"sender button %ld",(long)senderButton.tag);
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            // getting an NSString
            NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
            
            NSString * resendStr = [NSString stringWithFormat:@"resendemailbyid?changeit_id=%ld&app_name=%@&email_id_to=%@",(long)(senderButton.tag),kAppNameAPI,emailIdString];
            
                NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            
                AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
                manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
                [manager POST:resendStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSLog(@"response = %@", responseObject);
                    if ([[responseObject objectForKey:@"response"]isEqualToString:@"Email sent successfully"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES];
                        });
                        [self errorAlertWithTitle:kAppNameAlert message:@"Email Resend Successfully.If you don’t receive a confirmation email within 1 hour please contact us" actionTitle:@"Done"];
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hideAnimated:YES];
                        });
                        [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK"];
                    }
                   
            
                }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          NSLog(@"error = %@", error);
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [hud hideAnimated:YES];
                          });
                          [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK"];
                      }];
            
    }];
        
        UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:resendAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
    }
}


-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

@end
