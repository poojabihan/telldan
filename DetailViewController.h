//
//  DetailViewController.h
//  ChangeItApp
//
//  Created by Tarun Sharma on 01/04/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
@interface DetailViewController : UIViewController<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageVw;

@property (weak, nonatomic) IBOutlet UILabel *dateAndTime;

@property (weak, nonatomic) IBOutlet UITextView *detailText;

@property NSString *date, *details, *imageStr,*locationStr;
@property NSString * imageName;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@end
